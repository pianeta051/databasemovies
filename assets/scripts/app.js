// Referencias DOM
const modalFormulario = document.getElementById('add-modal');
const addMovieBoton = document.querySelector('header > button');
const backdrop = document.getElementById('backdrop');
const botonCancelFormulario = document.querySelector('#add-modal .btn--passive');
const botonAdd = modalFormulario.querySelector('.modal__actions .btn--success');
const sectionNoHayPeliculas = document.getElementById('entry-text');
const ulListaPeliculas = document.getElementById('movie-list');
const modalEliminarPelicula = document.getElementById('delete-modal');

// Variables de estado
const peliculas = [];

// Funciones
const actualizarInterfazPeliculas = () => {
    limpiarPeliculas();
    if (peliculas.length === 0) {
        sectionNoHayPeliculas.style.display = 'block';
    } else {
        sectionNoHayPeliculas.style.display = 'none';

    }
};

const cerrarModalFormulario = () => {
    backdrop.classList.remove('visible');
    modalFormulario.classList.remove('visible');
    limpiarFormulario();
};

const cerrarModalEliminar = () => {
    modalEliminarPelicula.classList.remove('visible');
    backdrop.classList.remove('visible');
};

const cerrarTodosModal = () => {
    cerrarModalFormulario();
    cerrarModalEliminar();
};

const crearPelicula = (pelicula) => {
    const lipelicula = document.createElement('li');
    lipelicula.classList.add('movie-element');
    lipelicula.id = "movie-" + pelicula.id;
    ulListaPeliculas.prepend(lipelicula);
    const divImage = document.createElement('div');
    divImage.classList.add('movie-element__image');
    lipelicula.prepend(divImage);
    const imgImage = document.createElement('img');
    imgImage.src = pelicula.image;
    imgImage.alt = pelicula.titulo;
    divImage.prepend(imgImage);
    const divInfo = document.createElement('div');
    divInfo.classList.add('movie-element__info');
    divImage.after(divInfo);
    const h2Titulo = document.createElement('h2');
    h2Titulo.innerHTML = pelicula.titulo;
    divInfo.prepend(h2Titulo);
    const pPuntuacion = document.createElement('p');
    pPuntuacion.innerHTML = pelicula.puntuacion + "/5 stars";
    h2Titulo.after(pPuntuacion);
    lipelicula.addEventListener('click', eliminarPelicula.bind(this, pelicula.id));
};

const eliminarErrores = () => {
    const listaParrafosError = document.querySelectorAll('.error');
    for (const nombreError of listaParrafosError) {
        nombreError.remove();
    }
};

const eliminarPelicula = (idPelicula) => {

    modalEliminarPelicula.classList.add('visible');
    backdrop.classList.add('visible');
    const botonCancelModalEliminar = modalEliminarPelicula.querySelector('.btn--passive');
    botonCancelModalEliminar.addEventListener('click', cerrarModalEliminar);
    // Referencia al boton Yes
    // Evento click
    // Dentro del evento: eliminar la pelicula del ARRAY peliculas
    // Usar la funcion filter y el idPelicula
    const botonYesModalEliminar = modalEliminarPelicula.querySelector('.btn--danger');
    botonYesModalEliminar.addEventListener('click', () => {
        // peliculas = peliculas.filter(pelicula => pelicula.id !== idPelicula);
        const posicionPeliculaEliminada = peliculas.findIndex(pelicula => pelicula.id === idPelicula);
        peliculas.splice(posicionPeliculaEliminada, 1);
        limpiarPeliculas();
        cerrarModalEliminar();
        mostrarListaPeliculas();
    });
};

const limpiarFormulario = () => {
    const inputs = modalFormulario.querySelectorAll('input');
    for (const input of inputs) {
        input.value = "";
    }
};

const limpiarPeliculas = () => {
    const elementosPeliculas = ulListaPeliculas.querySelectorAll('li');
    for (const elemento of elementosPeliculas) {
        elemento.remove();
    }
};

const mostrarErrores = (errores) => {
    for (const nombre in errores.mensajes) {
        const erroresPorNombre = errores.mensajes[nombre];
        for (const mensaje of erroresPorNombre) {
            const parrafo = document.createElement('p');
            parrafo.classList.add('error');
            parrafo.innerText = mensaje;
            const input = modalFormulario.querySelector(`[name= "${nombre}"]`);
            input.after(parrafo);
        }
    }
};

const mostrarListaPeliculas = () => {
    // for (const pelicula of peliculas) {
    //     crearPelicula(pelicula);
    // }
    const listaPeliculas = peliculas.map(pelicula => {
        return crearPelicula(pelicula);
    });
};

const obtenerValorInput = (nombre) => {
    const input = modalFormulario.querySelector(`[name="${nombre}"]`);
    return input.value.trim();
};

const validarDatos = (titulo, imagen, puntuacion) => {
    const errores = {
        length: 0,
        mensajes: {
            title: [],
            'image-url': [],
            rating: []
        }
    };
    if (titulo.length === 0) {
        errores.length++;
        errores.mensajes.title.push('Titulo no puede esta en blanco');
    }
    if (imagen.length === 0) {
        errores.length++;
        errores.mensajes['image-url'].push('Imagen no puede esta en blanco');
    }
    if (puntuacion.length === 0) {
        errores.length++;
        errores.mensajes['rating'].push('Puntuacion no puede esta en blanco');
    }
    if (isNaN(puntuacion)) {
        errores.length++;
        errores.mensajes['rating'].push('Puntuacion tiene que ser un numero');
    }
    if (puntuacion < 1 || puntuacion > 5) {
        errores.length++;
        errores.mensajes['rating'].push('Puntuacion tiene que estar en el rango de 1 a 5');
    }
    return errores;
};

// Eventos
addMovieBoton.addEventListener('click', () => {
    backdrop.classList.add('visible');
    modalFormulario.classList.add('visible');

});

botonCancelFormulario.addEventListener('click', cerrarModalFormulario);

botonAdd.addEventListener('click', () => {
    eliminarErrores();
    const valorInputTitle = obtenerValorInput('title');
    const valorInputImage = obtenerValorInput('image-url');
    const valorInputRating = obtenerValorInput('rating');
    const errores = validarDatos(valorInputTitle, valorInputImage, valorInputRating);
    if (errores.length != 0) {
        mostrarErrores(errores);
    } else {
        const pelicula = {
            titulo: valorInputTitle,
            image: valorInputImage,
            puntuacion: valorInputRating,
            id: peliculas.length
        };
        peliculas.push(pelicula);
        actualizarInterfazPeliculas();
        cerrarModalFormulario();
        mostrarListaPeliculas();
    }
});

backdrop.addEventListener('click', cerrarTodosModal);